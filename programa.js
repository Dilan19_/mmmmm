class Evento {

    generarBoton() {
        let contenedor;
        let boton;
        contenedor = document.getElementById("contenedor");

        boton = document.createElement("input");
        boton.setAttribute("type", "button");
        boton.setAttribute("value", "Pasa por aquí");
        boton.setAttribute("id", "boton");
        boton.addEventListener("mouseover", this.mostrarConsola);
        contenedor.appendChild(boton);
    }

    mostrarConsola() {
        console.log("Por encima");
    }

}

let miEvento = new Evento();
miEvento.generarBoton();
